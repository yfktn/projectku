<?php namespace Yfktn\ProjectKu\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Target extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'the-target' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Yfktn.ProjectKu', 'project', 'menu-target');
    }
}
