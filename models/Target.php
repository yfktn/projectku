<?php namespace Yfktn\ProjectKu\Models;

use Model;

/**
 * Model
 */
class Target extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'yfktn_projectku_target';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'perihal' => 'required|min:20',
        'deadline' => 'required'
    ];
    
    public $belongsTo = [
        'project' => ['Yfktn\ProjectKu\Models\Project', 'key'=>'project_id']
    ];
}
