<?php namespace Yfktn\ProjectKu\Models;

use Model;

/**
 * Model
 */
class Project extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'yfktn_projectku_project';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'nama' => 'required|min:30'
    ];
    
    public $hasMany = [
        'target' => ['Yfktn\ProjectKu\Models\Target', 'key'=>'project_id']
    ];
    
    public function filterFields($fields, $context = null ) {
        // hanya tampilkan relation manager pada saat project sudah dibuat
        if(!$this->exists) {
            // bila bukan update, jangan tampilkan!
            $fields->target->hidden = true;
        }
    }
}
