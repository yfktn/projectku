<?php namespace Yfktn\ProjectKu\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateYfktnProjectkuTarget extends Migration
{
    public function up()
    {
        Schema::create('yfktn_projectku_target', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('perihal', 250);
            $table->date('deadline');
            $table->text('penjelasan');
            $table->integer('project_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('yfktn_projectku_target');
    }
}
