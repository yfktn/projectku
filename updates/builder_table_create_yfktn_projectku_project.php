<?php namespace Yfktn\ProjectKu\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateYfktnProjectkuProject extends Migration
{
    public function up()
    {
        Schema::create('yfktn_projectku_project', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('nama', 200);
            $table->text('keterangan')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('yfktn_projectku_project');
    }
}
