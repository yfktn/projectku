<?php namespace Yfktn\ProjectKu\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateYfktnProjectkuTarget extends Migration
{
    public function up()
    {
        Schema::table('yfktn_projectku_target', function($table)
        {
            $table->integer('project_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('yfktn_projectku_target', function($table)
        {
            $table->integer('project_id')->nullable(false)->change();
        });
    }
}